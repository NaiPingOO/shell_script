#!/bin/bash
#初始化配置，安装默认组件
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
yum clean all
yum makecache

if [ $? -eq 0 ];then
	yum install lrzsz wget net-tools vim -y
	rpm -qa|egrep "lrzsz|wget|vim|net-tools" &>/dev/null
	if [ $? -eq 0 ];then
		echo "lrzsz、wget、vim、net-tools已经安装成功"
		exit 0
	else
		echo "安装失败，请检查"
		exit 1
	fi
fi