#!/bin/bash
[ -f /etc/init.d/functions ] && . /etc/init.d/functions
if [ $# -ne 1 ];then
	echo "请输入一个网址"
	exit 1
fi
curl -o /dev/null --connect-timeout 2 -sL $1 -w "%{http_code}\n" &>/dev/null
if [ $? -eq 0 ];then
	action "$1 is" /bin/true
else
	action "$1 is" /bin/false
fi