#!/bin/bash
if [  $# -eq 0 ];then
	echo "请输入1或者2个参数"
	exit 1
fi

case $1 in
    --add)
	     for n in `echo "$2 $3 $4 $5 $6 $7 $8 $9"|xargs -n 1`;do
         id $n &>/dev/null
	     if [ $? -ne 0 ];then
		      useradd $n &>/dev/null
		      echo "$n"|passwd --stdin $n &>/dev/null
		      echo "$n 用户添加成功，密码是：$n"
	     else
		      echo "$n 此用户存在"
             fi
         done
         ;;
    --del)
         for n in `echo "$2 $3 $4 $5 $6 $7 $8 $9"|xargs -n 1`;do
         id $n &>/dev/null
	     if [ $? -eq 0 ];then
		      userdel -r $n &>/dev/null
		      echo "$n 用户删除成功"
	     else
		      echo "$n 此用户不存在"
	     fi
         done
         ;;
    --help)
         echo "添加用户 --add 用户名"
         echo "删除用户 --del 用户名"
         exit 0
         ;;
     *)
         echo "USAGE:$0 --add|--del|--help"
         ;;
esac