#!/bin/bash
##################
#qq:837367121
#################
# Comments to support chkconfig on RedHat Linux
# chkconfig: 2345 64 36
[ -f /etc/init.d/functions ] && . /etc/init.d/functions
pidfile=/var/run/rsyncd.pid
judge() {
	RETVAL=$?
	if [ $RETVAL -eq 0 ];then
		action "rsync is $1" /bin/true
	else
		action "rsync is $1" /bin/false
	fi
	    return $RETVAL
 }

start() {
	if [ -f $pidfile ];then
		echo "rsync server is running..."
	else
		rsync --daemon
		judge started
	fi
	 return $RETVAL
}

stop() {
	if [ ! -f $pidfile ];then
		echo "rsync is already stopped"
		RETVAL=$?
	else
		kill -9 $(cat $pidfile)
		rm -f $pidfile
		judge stopped
	fi
	 return $RETVAL
}

case "$1" in
	start)
          start
          RETVAL=$?
          ;;
    stop)
          stop
          RETVAL=$?
          ;;
    restart)
          stop
          sleep 3
          start
          RETVAL=$?
          ;;
    *)
       echo "USAGE:$0 {start|stop|restart}"
       exit 1
esac
exit $RETVAL