#!/bin/bash
name=`grep "^$1" /etc/passwd|awk -F ':' '{print $1}'`
uid=`grep "^$1" /etc/passwd|awk -F ':' '{print $3}'`
if [ $# -ne 1 ];then
        echo "请至少输入一个参数"
        exit 1
fi

if [ "$1" == "$name" ];then
        echo "Hello $1,your UID is $uid"
else
        echo "该系统中没有这个用户"
fi

uid_num=`awk -F ':' '{print $1}' /etc/passwd|wc -l`
echo "当前用户数量为：$uid_num"
