#!/bin/bash
dir=/tmp/disk
time=`date +%F`

[ ! -d $dir ] && mkdir -p $dir
df -h > $dir/df_h_$time.log
df -i > $dir/df_i_$time.log

if [ `df -h|sed '1d'|awk "NR==1"|awk '{print $5}'|cut -d '%' -f1` -gt 85 ];then
	echo "磁盘空间已经大于85%"
elif [ `df -i|sed '1d'|awk "NR==1"|awk '{print $5}'|cut -d '%' -f1` -gt 85 ];then
	echo "磁盘inode已经大于85%"
else
	echo "磁盘空间正常"
fi