#!/bin/bash
backupdir=/home/backup
user=rsync_backup
backupserver=192.168.89.12
module=backup
passfile=/etc/reync.password

rsync -az --no-o --no-g $backupdir/ $user@$backupserver::$module/ --password-file=$passfile