#!/bin/bash
#mv wiki_log
cd /iflytek/nginx/logs
mv wiki_access.log wiki_access_$(date +%F -d "-1days").log
/iflytek/nginx/sbin/nginx -s reload
find /iflytek/nginx/logs -type f -name "wiki_access_*" -mtime +7|xargs rm -f