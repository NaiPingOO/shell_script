#!/bin/bash
version=`awk -F 'release' '{print $2}' /etc/redhat-release |cut -d '.' -f1|cut -d ' ' -f2`
server_5_6() {
 	while true
do
   if [ `ss -lntp|grep 21|wc -l` -ne 1 ];then
        /etc/init.d/vsftpd restart
   fi
 sleep 60
done
}
server_7() {
	while true
do
   if [ `ss -lntp|grep 21|wc -l` -ne 1 ];then
        systemctl restart vsftpd
   fi
sleep 60
done
}

case $version in
	5|6)
         server_5_6
         ;;
    *)
         server_7
         ;;
esac