#!/bin/bash
while true;do
      cat /root/1.log |awk '{print $1}'|sort |uniq -c|sort -rn|head -10 >/tmp/tmp.log
      exec < /tmp/tmp.log
      while read line;do
            IP=`echo $line|awk '{print $2}'`
            count=`echo $line|awk '{print $1}'`
            if [ $count -gt 50 ] && [ `iptables -L -n|grep "$IP"|wc -l` -lt 1 ];then
                  iptables -I INPUT -s $IP -j DROP
                  echo "$line is dropped" >> /tmp/droplist_$(date +%F).log
            fi
      done
sleep 180
done
