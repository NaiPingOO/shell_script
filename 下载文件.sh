#!/bin/bash
if [ $# -ne 2 ];then
        echo "请输入两个参数，第一个参数是网址，第二个参数是目录"
        exit 1
fi

if [ ! -d $2 ];then
     echo "此目录不存在请创建"
     read -p "是否创建此目录: " dir
     case $dir in
   	    y|Y)
            mkdir -p $2
            cd $2
            wget $1
            ;;
        n|N)
            exit 51
            ;;
        *)
            echo "USAGE:y|Y or n|N"
            ;;
     esac
else
	cd $2
	wget $1
	if [ $? -eq 0 ];then
		exit 0
	else
		echo "下载失败"
		exit 52
	fi
fi