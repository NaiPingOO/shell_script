#!/bin/bash
if [ $# -eq 0 ];then
    echo "请输入选项:'--add','--del','--help'"
    exit 1
fi

case $1 in
        --add)
              if [ $# -gt 2 ];then
                    echo "添加用户请以user1,user2,user3的方式添加"
                    exit 1
              fi
              echo $2|sed 's#,# #g'|xargs -n 1 > /tmp/useradd.txt
              exec < /tmp/useradd.txt
              while read line;do
                    id $line &> /dev/null
                    if [ $? -eq 0 ];then
                         echo "$line 此用户已经添加"
                         exit 2
                    else
                         useradd $line
                         echo "$line"|passwd --stdin $line
                    fi
              done
              ;;
        --del)
              if [ $# -gt 2 ];then
                    echo "添加用户请以user1,user2,user3的方式添加"
                    exit 1
              fi
              echo $2|sed 's#,# #g'|xargs -n 1 > /tmp/userdel.txt
              exec < /tmp/userdel.txt
              while read line;do
                    id $line &> /dev/null
                    if [ $? -eq 1 ];then
                         echo "$line,此用户不存在，无法删除"
                         exit 2
                    else
                         userdel -r $line
                         echo "删除$line 用户成功"
                    fi
              done
              ;;
        --help)
              echo "请输入--add,--del"
              exit 2
              ;;
            *)
              echo "只能输入--add,--del,--help这三个参数"
              exit 2
              ;;
esac
