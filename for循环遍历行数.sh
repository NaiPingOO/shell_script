#!/bin/bash
if [ $# -ne 1 ];then
     echo "请输入一个参数"
     exit 1
fi

if [ ! -f $1 ];then
     echo "这个参数只能是文件"
     exit 2
fi

for n in `cat $1`;do
      file=`echo $n|sed 's#[^0-9]##g'|wc -L`
      if [ $file -eq 1 ];then
           echo $n
      fi
done
