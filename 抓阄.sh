#!/bin/bash
> /tmp/xujun.txt
while true;do
    file=/tmp/xujun.txt
    [ ! -f $file ] && touch $file
    read -p "请输入您的名字: " name
    if [ -z $name ];then
        echo "不能输入为空"
        continue
    fi
    if [ $name == q ] || [ $name == Q ];then
        exit 0
    fi
    n=`echo $name|sed 's#[0-9a-zA-Z]##g'`
    if [ ! -z $n  ];then
        echo "请输入大小写英文或者数字，不可以是特殊符号"
        continue
    else
        shuzi=`echo $(($RANDOM%100))`
        name1=`grep -w $name $file |awk '{print $1}'`
        if [ -z $name1 ];then
            echo "用户$name对应的数字是： $shuzi"
            echo "$name $shuzi" >>/tmp/xujun.txt
        else
            echo "此用户名已存在，请输入其他的用户"
            continue
        fi
    fi
done
