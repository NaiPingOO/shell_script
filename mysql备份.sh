#!/bin/bash
path=/home/backup
MYUSER=root
MYPASS=123456
MYCMD="mysql -u$MYUSER -p$MYPASS"
MYDUMP="mysqldump -u$MYUSER -p$MYPASS -x -B -F -R"
[ ! -d $path ] && mkdir -p $path
DBLIST=`$MYCMD -e "show databases;"|sed 1d|egrep "^wiki$"`
for dbname in $DBLIST;do
	[ ! -d $path/$dbname ] && mkdir -p $path/$dbname
    $MYDUMP $dbname|gzip >$path/$dbname/${dbname}_$(date +%F).sql.gz
done
rsync -az --no-o --no-g /home/backup/ rsync_backup@192.168.89.12::backup/ --password-file=/etc/rsync.password
