#!/bin/bash
exec < /root/1.txt
while read line;do
     dir=$(echo $line |awk '{print $1}')
     file=$(echo $line |awk '{print $3}')
     name=$(echo $line |awk '{print $2}')
     if [ ! -d /backup/$dir ];then
           mkdir -p /backup/$dir
     else
           cd /backup/$dir
           touch $file
           mv $file $name.jpg
     fi
done
