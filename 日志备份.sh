#!/bin/bash
cd /home
tar zcvf /backup/log_227/log_$(date +%F).tar.gz logs/ &>/dev/null
tar zcvf /backup/log_227/eds_$(date +%F).tar.gz eds2.0/ &>/dev/null
find /backup/ -type f -name "*.tar.gz" -mtime +90|xargs rm -f
rsync -az --no-o --no-g /backup/ rsync_backup@11.7.22.179::backup/ --password-file=/etc/rsync.password
