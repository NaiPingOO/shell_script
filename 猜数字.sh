#!/bin/bash
[ ! -z /tmp/xujun.txt ] && > /tmp/xujun.txt
n=`echo $(($RANDOM%101))`
while true;do
     read -p "请输入数字，一共5次机会: " shuzi
     if [ -z $shuzi ];then
          echo "必须需要输入一个数字"
          continue
     fi
     expr $shuzi + 1 &>/dev/null
     if [ $? -ne 0 ];then
          echo "请重新输入一个正整数"
          continue
     fi
     if [ $shuzi -ge 100 ];then
          echo "请输入的数字要小于100，在1~99之间选择"
          continue
     fi
     echo $shuzi >>/tmp/xujun.txt
     n1=`cat /tmp/xujun.txt|wc -l`
     if [ $n1 -gt 5 ];then
           echo "你的剩余机会为0，游戏失败"
           echo "数字为$n"
           break
     fi
     if [ $shuzi -eq $n ];then
           echo "你输入的数字$shuzi正确，恭喜闯关成功"
           exit 0
     elif [ $shuzi -gt $n ];then
          echo "你输入的$shuzi大于这个数"
     else
          echo "你输入的$shuzi小于这个数"
     fi
done
> /tmp/xujun.txt