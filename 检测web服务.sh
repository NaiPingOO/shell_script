#!/bin/bash
[ -f /etc/init.d/functions ] && . /etc/init.d/functions
shuzi=`netstat -lntp|grep 80|wc -l`
web=`netstat -lntp|grep 80|egrep -o "nginx|httpd"`
if [ $shuzi -gt 0 ];then
	action "此服务器有web服务" /bin/true
  if [ $web = nginx ];then
 	action "此web服务是Nginx" /bin/true
  else
  	action "此web服务是httpd" /bin/true
  fi
 else
 	action "此服务器没有web服务" /bin/false
fi