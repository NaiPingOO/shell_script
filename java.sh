﻿#!/bin/bash
#加载环境变量
source /etc/profile
exist_rpms=`ps -ef|grep ts-service-5.0-ty4.jar|grep -v grep|awk '{print $2}'|wc -l`
if [ $exist_rpms -eq 1 ];then
	kill -9 $exist_rpms
	cd /home/ts-service
    nohup java -jar ts-service-5.0-ty4.jar >>/home/ts-service/nohup.out 2>&1 &
fi