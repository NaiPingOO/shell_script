#!/bin/bash
RED_COLOR='\E[1;31m'
GREEN_COLOR='\E[1;32m'
YELLOW_COLOR='\E[1;33m'
RES='\E[0m'

if [ $# -ne 2 ];then
	echo "请输入两个参数"
	exit 1
fi

case $2 in
	red)
	   echo -e "${RED_COLOR}$1${RES}"
	   ;;
	green)
	   echo -e "${GREEN_COLOR}$1${RES}"
	   ;;
	yellow)
	   echo -e "${YELLOW_COLOR}$1${RES}"
	   ;;
	*)
       echo "USAGE:$0 red|green|yellow" 
esac