#!/bin/bash
path=/home/backup
MYUSER=root
MYPASS=123456
MYCMD="mysql -u$MYUSER -p$MYPASS"
MYDUMP="mysqldump -u$MYUSER -p$MYPASS -x -B -F -R"
[ ! -d $path ] && mkdir -p $path
DBLIST=`$MYCMD -e "show databases;"|sed 1d|egrep "^wiki$"`
for dbname in $DBLIST;do
	[ ! -d $path/$dbname ] && mkdir -p $path/$dbname
    $MYDUMP $dbname|gzip >$path/$dbname/${dbname}_$(date +%F).sql.gz
    DBTABLE=`mysql -uroot -p123456 -e "use wiki;show tables"|sed 1d`
    for tablename in $DBTABLE;do
    $MYDUMP $dbname $tablename|gzip >$path/$dbname/${tablename}_$(date +%F).sql.gz
    done
done