#!/bin/bash
if [ $# -ne 1 ];then
	echo "请输入软件包名"
	exit 1
fi

if rpm -q $1 &>/dev/null; then
    echo "$1 已经安装"
else
    echo "$1 未安装!"
fi