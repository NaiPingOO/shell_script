#!/bin/bash
#监控cpu利用率
DATE=`date +%F`
IP=`ifconfig ens33 |awk -F '[ :]+' '/inet /{print $3}'`
#只支持CentOS7
if ! which vmstat &>/dev/null;then
    echo "vmstat command no found, Please install procps package."
    exit 1
fi
US=$(vmstat |awk 'NR==3{print $13}')
SY=$(vmstat |awk 'NR==3{print $14}')
USE=$(($US+$SY))
if [ $USE -ge 50 ];then
    echo "date: $DATE  ip: $IP  当前服务器CPU利用率已经大于50，请处理"
else
	echo "当前服务器CPU利用率低于50"
fi
