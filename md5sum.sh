#!/bin/bash
path=/var/html/www
[ ! -d /test/md5 ] && mkdir -p /test/md5
md5_log=/test/md5/md5_$(date +%F).log
num_log=/test/md5/num_$(date +%F).log
num=`cat $num_log|wc -l`

while true
do
	log=/test/md5/check.log
	[ ! -f $log ] && touch $log
	md5_count=`md5sum -c $md5_log 2>/dev/null|grep FAILED|wc -l`
	num_count=`find $path -type f|wc -l`
	find $path -type f >/test/md5/new.log
        if [ $md5_count -ne 0 ] || [ $num_count -ne $num ];then
         echo "$(md5sum -c $md5_log 2>/dev/null|grep FAILED)" >>$log
         diff $num_log /test/md5/new.log >>$log
         echo  "网站被入侵，请及时发现" >>/tmp/web.txt
        fi
        sleep 180
done