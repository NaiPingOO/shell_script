#!/bin/bash
if [ $# -ne 2 ];then
	echo "请输入至少两个参数"
	exit 1
fi

expr $1 + $2 + 0 &>/dev/null
if [ -$? -ne 0 ];then
	echo "请输入正整数"
    exit 1
fi

echo "$1+$2"=$(($1+$2))
echo "$1-$2"=$(($1-$2))
echo "$1*$2"=$(($1*$2))
echo "$1/$2"=$(($1/$2))