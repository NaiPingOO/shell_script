#!/bin/bash
if [ $# -eq 0 ];then
     echo "当前目录下: "
     ls .
else
     for dir in $@;do
         if [ -d $dir ];then
             echo "此目录下有这些子目录: "
             find $dir -type d
         else
             echo "此目录不存在 $dir"
         fi
     done
fi