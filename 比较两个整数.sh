#!/bin/bash
if [ $# -ne 2 ];then
    echo "请输入两个参数"
    exit 1
fi

num1=$1
num2=$2
expr $num1 + $num2 + 10 &>/dev/null
if [ $? -ne 0 ];then
    echo "请输入两个整数"
    exit 2
fi

if [ $num1 -gt $num2 ];then
   echo "$num1 > $num2"
elif [ $num1 -eq $num2 ];then
   echo "$num1 = $num2"
else
   echo "$num1 < $num2"
fi