#!/bin/bash
#用途：防止DDos攻击，将访问量超过100的IP地址禁掉
#作者：Caron maktini
#日期：2018年10月17日
#版本：v0.1

t1=`date -d "-1 min" +%Y:%H:%M`
log=/data/logs/access_log

block_ip()
{
    egrep "$t1:[0-5]+"  $log > /tmp/tmp_last_min.log

    #把一分钟内访问量高于100的ip地址记录到一个临时文件中。
    awk '{print $1}'  /tmp/tmp_last_min.log | sore -n | uniq -c | awk '$1>100 {print $2}' > /tmp/bad_ip.list

    #计算ip的数量
    n=`wc -l /tmp/bad_ip.list | awk '{print $1}'`
    
    #当ip数大于0时，才会用iptables封掉。
    if [ $n -ne 0 ]
    then 
        for ip in `cat /tmp/bad_ip.list`
        do 
           iptables -I INPUT -s $ip -j REJCT
        done
     
       #将这些被封的ip记录到日志里
       echo "`date` 封掉的ip有： "  >> /tmp/black_ip.log
       echo /tmp/bad_ip.list  >> /tmp/black_ip.log
   fi
 }

unblock_ip ()
{
    #首先将包括个数小于5的ip记录到一个临时文件里，把它们标记为白名单IP

    iptables -nvL INPUT | sed '1d' | awk '$1<5 {print $8}' > /tmp/good_ip.list
   n=`wc -l  /tmp/good_ip.list | awk '{print $1}'`
    if [ $n -ne 0 ]
    then 
        for ip in `cat  /tmp/good_ip.list`
        do
            iptables -D INPUT -s $ip -j REJECT
        done
        echo "`date` 解封的ip有："  >>  /tmp/unblock_ip.log
        cat   /tmp/good_ip.list  >> /tmp/unblock_ip.log
    fi
    #当解封完白名单ip后，将计数清零，进入下一个计数周期
    iptables  -Z
}

#取当前时间的分钟数

t=`date  +%M`
#当分钟数内为00或者30时（即每隔30分钟），执行解封ip的函数，其他时间执行解封ip的函数。

if [  $t  == "00" ] || [ $t == "30" ]
then 
      unblock_ip 
      block_ip
else
      block_ip

fi