#!/bin/bash
nginx_port=$(ss -lntup|grep 1028|wc -l)
nginx_ps=$(ps aux|grep nginx|grep -v grep|wc -l)
USP=$(ss -lntp|grep 48888|wc -l)
if [ $nginx_port -eq 0 ] || [ $nginx_ps -le 1 ] || [ $USP -eq 0 ];then
	echo "Nginx 1028端口挂了或者进程已经不在了,或者USP48888端口已经挂了"
        /etc/init.d/keepalived stop
        exit 1
fi
