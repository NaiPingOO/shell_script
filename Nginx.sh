#!/bin/bash
#chkconfig: 2345 40 80
#description：starts，stop and saves iptables firewall
[ -f /etc/init.d/functions ]&&. /etc/init.d/functions
pidfile=/iflytek/nginx/logs/nginx.pid
start(){
  if [ -f $pidfile ];then
    echo "nginx is running"
  else
    /iflytek/nginx/sbin/nginx &>/dev/null
    action "nginx is started" /bin/true
  fi
}  

stop(){
  if [  -f $pidfile ];then
    /iflytek/nginx/sbin/nginx -s stop &>/dev/null
    action "nginx is stopped" /bin/true
  else 
    action "nginx is stopped" /bin/false
  fi
}  

reload(){
   if [  -f $pidfile ];then
    /iflytek/nginx/sbin/nginx -s reload &>/dev/null
    action "nginx is reloaded" /bin/true
  else 
    echo "cat't open $pidfile,no such file or directory"
   fi
}

case $1 in 
     start)
          start
          RETVAL=$?
          ;;
     stop)
          stop
          RETVAL=$?
          ;;
     restart)
          stop
          sleep 3
          start
          RETVAL=$?
          ;;
      reload)
          reload
          RETVAL=$?
          ;;
      *)
        echo "$0 {start|stop|restart}"
        exit 1
esac
exit $RETVAL