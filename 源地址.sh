#!/bin/bash
dir=/opt/data2
[ -d $dir ] && cd $dir
tar zcf tupian.tar.gz `find data/ -type f -name "*.jpg" -mtime -7`
scp -r tupian.tar.gz root@11.7.22.230:/opt/fdfs/data2/
find /opt/data2 -type f -name "tupian.tar.gz"|xargs rm -f