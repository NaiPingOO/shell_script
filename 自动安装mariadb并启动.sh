#!/bin/bash
rpm -qa|grep mariadb &>/dev/null
if [ $? -ne 0 ];then
      yum install mariadb* -y
      systemctl start mariadb
      systemctl status mariadb
else
      systemctl status mariadb
      if [ $? -ne 0 ];then
           systemctl start mariadb
           systemctl status mariadb
      fi
fi
