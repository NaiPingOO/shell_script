#!/bin/bash
hostName=`hostname`
if [ -z $hostName ];then
     hostname www.magedu.com
elif [ $hostName == "localhost.localdomain" ];then
     hostname www.magedu.com
elif [ $hostName == "localhost" ];then
     hostname www.magedu.com
else
     hostname
fi