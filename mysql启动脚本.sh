#!/bin/bash
#mysql启动脚本
#chkconfig:- 85 15
#description：starts，stop and mysql
user=root
password=123456
mysql_start ()
{
           if [ `netstat -lntup|grep 3306|wc -l` -eq 0 ];then
               printf "starting mysql...\n"
               /iflytek/mysql/bin/mysqld_safe  &>/dev/null &
           else
               printf "mysql is running...\n"
           fi
}

mysql_stop ()
{
           if [ ! `netstat -lntup|grep 3306|wc -l` -eq 0 ];then
               /iflytek/mysql/bin/mysqladmin -u$user -p$password shutdown &>/dev/null
              echo "mysql is stoping...."
              else
              echo "mysql is starting..."
           fi
}

case $1 in
start)
      mysql_start
      ;;
stop)
      mysql_stop
      ;;
restart)
      mysql_stop
      sleep 2
      mysql_start
      ;;
*)
      echo "UGE:$0{start|stop|restart}"
      ;;
esac