#!/bin/bash
ip add|awk -F ': ' '$1 ~ "^[1-9]" {print $2}' >/tmp/ens.list
while true;do
      read -p "请输入对应的网卡名称 `cat /tmp/ens.list|xargs`: " ens
      if [ -z "$ens" ];then
         echo "不能为空。请输入对应的网卡名字"
      fi
      if ! grep -qw "$ens" /tmp/ens.list;then
           echo "输入的网卡名称是错误的"
      else
           ens1=`ip add show dev "$ens"|awk "NR==3"|awk '{print $2}'|cut -d "/" -f1`
           echo -e "\033[31m$ens1 \033[0m"
           break
      fi
done