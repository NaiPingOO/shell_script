#!/bin/bash
#批量分发脚本
. /etc/init.d/functions
if [ $# -ne 2 ];then
      echo "请输入2个参数，否则视为无效"
      exit 1
fi

for IP in 41 31;do
      scp -rp $1 root@192.168.89.$IP:$2 &>/dev/null
      if [ $? -eq 0 ];then
             echo "传输成功" /usr/bin/true
      else
             echo "传输失败" /usr/bin/false
      fi
done
