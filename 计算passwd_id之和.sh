#!/bin/bash
id1=`awk 'NR==10' /etc/passwd|awk -F ":" '{print $3}'`
id2=`awk 'NR==20' /etc/passwd|awk -F ":" '{print $3}'`

id_sum=$(($id1+id2))
echo "id1+id2="$id_sum