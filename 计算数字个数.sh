#!/bin/bash
if [ $# -ne 1 ];then
     echo "请输入一个参数即可"
     exit 1
fi
sum=0
exec < $1
while read line;do
    echo $line|sed 's#[a-zA-Z]##g'|wc -L
    num=$(echo $line|sed 's#[a-zA-Z]##g'|wc -L)
    for n in $num;do
         sum=$((sum+n))
    done
done
echo "sum:$sum"